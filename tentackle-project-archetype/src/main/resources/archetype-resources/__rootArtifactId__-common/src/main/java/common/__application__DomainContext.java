#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.common;

import org.tentackle.pdo.DefaultDomainContext;
import org.tentackle.session.Session;

/**
 * Application specific domain context.
 */
public class ${application}DomainContext extends DefaultDomainContext {

  private static final long serialVersionUID = 1L;

  /**
   * Creates a domain context.
   *
   * @param session the session
   * @param sessionImmutable true if session cannot be changed anymore
   */
  public ${application}DomainContext(Session session, boolean sessionImmutable) {
    super(session, sessionImmutable);
  }

  /**
   * Creates a mutable domain context.
   *
   * @param session the session
   */
  public ${application}DomainContext(Session session) {
    super(session);
  }

}
