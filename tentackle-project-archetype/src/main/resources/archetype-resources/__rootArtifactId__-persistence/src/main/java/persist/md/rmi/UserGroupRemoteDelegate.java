#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.persist.md.rmi;

import ${package}.pdo.md.UserGroup;
import ${package}.persist.md.UserGroupPersistenceImpl;


/**
 * Remote delegate for {@link UserGroupPersistenceImpl}.
 */
public interface UserGroupRemoteDelegate
       extends OrgUnitRemoteDelegate<UserGroup,UserGroupPersistenceImpl> {

  // @wurblet inclrmi Include --missingok .${symbol_dollar}classname/methods

//<editor-fold defaultstate="collapsed" desc="code 'inclrmi' generated by wurblet Include">//GEN-BEGIN:inclrmi


//</editor-fold>//GEN-END:inclrmi

}
