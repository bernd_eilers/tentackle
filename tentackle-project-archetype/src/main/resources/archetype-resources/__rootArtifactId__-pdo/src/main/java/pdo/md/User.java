#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.pdo.md;

import ${package}.pdo.md.domain.UserDomain;
import ${package}.pdo.md.persist.UserPersistence;

import org.tentackle.pdo.Plural;
import org.tentackle.pdo.Singular;
import org.tentackle.session.ClassId;
import org.tentackle.session.TableName;

/*
 * @{
 * tablename = md.user
 * classid   = 1001
 * mapping   = ${symbol_dollar}model/${symbol_dollar}tablename.map
 * @}
 */

/*
 * @> ${symbol_dollar}mapping
 *
 * ${symbol_pound} User
 * name := ${symbol_dollar}classname
 * table := ${symbol_dollar}tablename
 * id := ${symbol_dollar}classid
 * extends := OrgUnit
 * integrity := ${symbol_dollar}integrity
 *
 * ${symbol_pound}${symbol_pound} attributes
 * String(64)   password                      password        hashed password [MUTE]
 * boolean      loginAllowed                  login_allowed   true if login is allowed
 * boolean      passwordChangeable            passwd_chgbl    user allowed to change its own password?
 * boolean      changingPreferencesAllowed	  prefs_chgbl     user allowed to maintain its own preferences?
 * boolean      systemPreferencesOnly         prefs_system    user restricted to system preferences only?
 *
 * ${symbol_pound}${symbol_pound} indexes
 *
 * ${symbol_pound}${symbol_pound} relations
 * User2Group:
 *    relation = composite list,
 *    name = nmLinks,
 *    method = UserId,
 *    nm = UserGroup UserGroups
 *
 * ${symbol_pound}${symbol_pound} validations
 *
 * @<
 */

/**
 * Organizational Unit User.
 * <p>
 * For security reasons, the password hash is stored in the user table, but is not part of the User PDO.
 */
@TableName(value =/**/"md.user"/**/, // @wurblet < Inject --string ${symbol_dollar}tablename
           mapSchema =/**/false/**/, // @wurblet < Inject ${symbol_dollar}mapSchema
           prefix =/**/""/**/)       // @wurblet < Inject --string ${symbol_dollar}tablePrefix
@ClassId(/**/1001/**/)               // @wurblet < Inject ${symbol_dollar}classid
@Singular("User")
@Plural("Users")
public interface User extends OrgUnit<User>, UserPersistence, UserDomain {

  // @wurblet(fold=expanded) modelComment ModelComment

  //<editor-fold defaultstate="expanded" desc="code 'modelComment' generated by wurblet ModelComment">//GEN-BEGIN:modelComment

  /*
   * -------------------------------------------------------------------------------------------------------------------
   *
   * User is referenced by:
   *
   * Message via orgUnitId as orgUnit [1:1]
   * UserGroup via User2Group as NmLinks [N:M]
   *
   *
   * User is a root entity
   *
   *
   * User is a composite with the components:
   *    + User2Group via ug.userId as nmLinks [N:M] to UserGroup as userGroups
   *
   *
   * User is referencing the following entities:
   *
   * UserGroup from User2Group via userGroupId [N:M]
   *
   *
   * Components of User are not deeply referenced
   *
   *
   * User is not extended
   *
   * -------------------------------------------------------------------------------------------------------------------
   */

  //</editor-fold>//GEN-END:modelComment

  // @wurblet uniqueDomainKey UniqueDomainKey

  //<editor-fold defaultstate="collapsed" desc="code 'uniqueDomainKey' generated by wurblet UniqueDomainKey">//GEN-BEGIN:uniqueDomainKey

  // User inherits from OrgUnit

  //</editor-fold>//GEN-END:uniqueDomainKey
}
