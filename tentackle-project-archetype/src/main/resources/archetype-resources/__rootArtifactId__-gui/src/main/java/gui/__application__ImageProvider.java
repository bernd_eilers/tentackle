#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.gui;

import org.tentackle.fx.DefaultImageProvider;
import org.tentackle.fx.ImageProviderService;

/**
 * Application specific image provider.
 */
@ImageProviderService(${application}ImageProvider.REALM)
public class ${application}ImageProvider extends DefaultImageProvider {

  /**
   * The application's image realm.
   */
  public static final String REALM = "${application}";

}
