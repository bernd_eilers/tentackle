/*
 * Tentackle - https://tentackle.org.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.model.impl;

import org.tentackle.common.Constants;
import org.tentackle.model.DataType;
import org.tentackle.model.Entity;
import org.tentackle.model.EntityFactory;
import org.tentackle.model.EntityOptions;
import org.tentackle.model.ModelException;
import org.tentackle.model.SourceInfo;
import org.tentackle.model.TrackType;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity options.
 *
 * @author harald
 */
public class EntityOptionsImpl extends CommonOptionsImpl implements EntityOptions {

  /** don't apply model defaults. */
  public static final String OPTION_NO_MODELDEFAULTS = "NODEFAULTS";

  /** don't generate ID as primary key. */
  public static final String OPTION_NO_PRIMARY = "NOPKEY";

  /** column tableserial is provided. */
  public static final String OPTION_TABLESERIAL = "TABLESERIAL";

  /** token lock columns editedBy/Since/Expiry are provided. */
  public static final String OPTION_TOKENLOCK = "TOKENLOCK";

  /** normText is provided. */
  public static final String OPTION_NORMTEXT = "NORMTEXT";

  /** root entity. */
  public static final String OPTION_ROOT = "ROOT";

  /** rootid is provided. */
  public static final String OPTION_ROOTID = "ROOTID";

  /** rootclassid is provided. */
  public static final String OPTION_ROOTCLASSID = "ROOTCLASSID";

  /** enable remoting. */
  public static final String OPTION_REMOTE = "REMOTE";

  /** entity provided by externally. */
  public static final String OPTION_PROVIDED = "PROVIDED";





  private final EntityFactory factory;
  private final Entity entity;

  private boolean noModelDefaults;
  private boolean noPrimary;
  private TrackType trackType;
  private boolean tableSerialProvided;
  private boolean tokenLockProvided;
  private boolean normTextProvided;
  private boolean rootEntity;
  private boolean rootIdProvided;
  private boolean rootClassIdProvided;
  private boolean remote;
  private boolean provided;
  private List<String> sorting;   // != null if '|' found



  /**
   * Creates entity options.
   *
   * @param factory the factory
   * @param entity the entity the options belong to
   * @param sourceInfo the source info
   */
  public EntityOptionsImpl(EntityFactory factory, Entity entity, SourceInfo sourceInfo) {
    super(sourceInfo);
    this.factory = factory;
    this.entity = entity;
    trackType = TrackType.NONE;
  }


  @Override
  public Entity getEntity() {
    return entity;
  }

  @Override
  public boolean noModelDefaults() {
    return noModelDefaults;
  }

  @Override
  public boolean applyOption(String option, Boolean ctrl) throws ModelException {

    boolean applied = super.applyOption(option, ctrl);
    boolean on = ctrl == null || ctrl;

    if (!applied) {
      String uco = option.toUpperCase();
      applied = true;
      switch (uco) {
        case OPTION_NO_MODELDEFAULTS:
          setNoModelDefaults(on);
          break;

        case OPTION_NO_PRIMARY:
          setNoPrimaryKey(on);
          break;

        case OPTION_ROOT:
          setRootEntity(on);
          break;

        case OPTION_REMOTE:
          setRemote(on);
          break;

        case OPTION_PROVIDED:
          setProvided(on);
          break;

        case OPTION_TABLESERIAL:
          setTableSerialProvided(on);
          break;

        case OPTION_ROOTID:
          setRootIdProvided(on);
          break;

        case OPTION_ROOTCLASSID:
          setRootClassIdProvided(on);
          break;

        case OPTION_TOKENLOCK:
          setEditedByProvided(on);
          break;

        case OPTION_NORMTEXT:
          setNormTextProvided(on);
          break;

        default:
          // sorted column?
          // this is just a convenience hack to allow ommitting the pipe sign.
          // works, however, only if the column to sort is not an option keyword.
          if (ctrl != null) {
            if (sorting == null) {
              sorting = new ArrayList<>();
            }
            sorting.add((on ? "+" : "-") + option);
            break;
          }
          else  {
            // tracking type?
            try {
              setTrackType(TrackType.valueOf(uco));
              break;
            }
            catch (IllegalArgumentException ex) {
              // ignore
            }
          }
          applied = false;
      }
    }
    return applied;
  }


  /**
   * Checks that the sorting string starts with a + or -.
   *
   * @param token the sorting token
   * @return true if applied
   */
  public boolean processSorting(String token) {
    boolean applied = false;
    if (sorting == null) {
      sorting = new ArrayList<>();
    }
    if (token != null && !token.isEmpty()) {
      // must start with + or -
      char c = token.charAt(0);
      if (c == '-' || c == '+') {
        sorting.add(token);
        applied = true;
      }
      else if (Character.isLetter(c)) {
        // prepend +
        sorting.add("+" + token);
        applied = true;
      }
    }
    return applied;
  }


  @Override
  public boolean isNoPrimaryKey() {
    return noPrimary;
  }

  @Override
  public TrackType getTrackType() {
    return trackType;
  }

  @Override
  public boolean isTableSerialProvided() {
    return tableSerialProvided;
  }

  @Override
  public boolean isTokenLockProvided() {
    return tokenLockProvided;
  }

  @Override
  public boolean isNormTextProvided() {
    return normTextProvided;
  }

  @Override
  public boolean isRoot() {
    return rootEntity;
  }

  @Override
  public boolean isRootIdProvided() {
    return rootIdProvided;
  }

  @Override
  public boolean isRootClassIdProvided() {
    return rootClassIdProvided;
  }

  @Override
  public boolean isRemote() {
    return remote;
  }

  @Override
  public boolean isProvided() {
    return provided;
  }

  @Override
  public List<String> getSorting() {
    return sorting;
  }


  public void setNoModelDefaults(boolean noModelDefaults) {
    this.noModelDefaults = noModelDefaults;
  }

  public void setTrackType(TrackType trackType) {
    this.trackType = trackType;
  }

  public void setEditedByProvided(boolean tokenLockProvided) {
    this.tokenLockProvided = tokenLockProvided;
    AttributeImpl editedByAttribute = (AttributeImpl) entity.getAttributeByJavaName(Constants.AN_EDITEDBY, false);
    AttributeImpl editedSinceAttribute = (AttributeImpl) entity.getAttributeByJavaName(Constants.AN_EDITEDSINCE, false);
    AttributeImpl editedExpiryAttribute = (AttributeImpl) entity.getAttributeByJavaName(Constants.AN_EDITEDEXPIRY, false);
    if (tokenLockProvided) {
      if (editedExpiryAttribute == null) {
        editedExpiryAttribute = (AttributeImpl) factory.createAttribute(entity, getSourceInfo(), true);
        editedExpiryAttribute.setDataType(DataType.TIMESTAMP);
        editedExpiryAttribute.setColumnName(Constants.CN_EDITEDEXPIRY);
        editedExpiryAttribute.setName(Constants.AN_EDITEDEXPIRY);
        editedExpiryAttribute.getOptions().setComment("editing lock expiration");
        editedExpiryAttribute.getOptions().setFromSuper(true);
        entity.getAttributes().add(0, editedExpiryAttribute);
      }
      if (editedSinceAttribute == null) {
        editedSinceAttribute = (AttributeImpl) factory.createAttribute(entity, getSourceInfo(), true);
        editedSinceAttribute.setDataType(DataType.TIMESTAMP);
        editedSinceAttribute.setColumnName(Constants.CN_EDITEDSINCE);
        editedSinceAttribute.setName(Constants.AN_EDITEDSINCE);
        editedSinceAttribute.getOptions().setComment("being edited since");
        editedSinceAttribute.getOptions().setFromSuper(true);
        entity.getAttributes().add(0, editedSinceAttribute);
      }
      if (editedByAttribute == null) {
        editedByAttribute = (AttributeImpl) factory.createAttribute(entity, getSourceInfo(), true);
        editedByAttribute.setDataType(DataType.LONG_PRIMITIVE);
        editedByAttribute.setColumnName(Constants.CN_EDITEDBY);
        editedByAttribute.setName(Constants.AN_EDITEDBY);
        editedByAttribute.getOptions().setComment("being edited by user id");
        editedByAttribute.getOptions().setFromSuper(true);
        entity.getAttributes().add(0, editedByAttribute);
      }
    }
    else {
      if (editedByAttribute != null) {
        entity.getAttributes().remove(editedByAttribute);
      }
      if (editedSinceAttribute != null) {
        entity.getAttributes().remove(editedSinceAttribute);
      }
      if (editedExpiryAttribute != null) {
        entity.getAttributes().remove(editedExpiryAttribute);
      }
    }
  }

  public void setNormTextProvided(boolean normText) {
    this.normTextProvided = normText;
    AttributeImpl normTextAttribute = (AttributeImpl) entity.getAttributeByJavaName(Constants.AN_NORMTEXT, false);
    if (normText) {
      if (normTextAttribute == null) {
        normTextAttribute = (AttributeImpl) factory.createAttribute(entity, getSourceInfo(), true);
        normTextAttribute.setDataType(DataType.STRING);
        normTextAttribute.setColumnName(Constants.CN_NORMTEXT);
        normTextAttribute.setName(Constants.AN_NORMTEXT);
        normTextAttribute.getOptions().setComment("normalized text");
        normTextAttribute.getOptions().setFromSuper(true);
        entity.getAttributes().add(0, normTextAttribute);
      }
    }
    else if (normTextAttribute != null) {
      entity.getAttributes().remove(normTextAttribute);
    }
  }

  public void setNoPrimaryKey(boolean noPrimary) {
    this.noPrimary = noPrimary;
  }

  public void setTableSerialProvided(boolean tableSerial) {
    this.tableSerialProvided = tableSerial;
    AttributeImpl tableSerialAttribute = (AttributeImpl) entity.getAttributeByJavaName(Constants.AN_TABLESERIAL, false);
    if (tableSerial) {
      if (tableSerialAttribute == null) {
        tableSerialAttribute = (AttributeImpl) factory.createAttribute(entity, getSourceInfo(), true);
        tableSerialAttribute.setDataType(DataType.LONG_PRIMITIVE);
        tableSerialAttribute.setColumnName(Constants.CN_TABLESERIAL);
        tableSerialAttribute.setName(Constants.AN_TABLESERIAL);
        tableSerialAttribute.getOptions().setComment("table serial");
        tableSerialAttribute.getOptions().setFromSuper(true);
        entity.getAttributes().add(0, tableSerialAttribute);
      }
    }
    else if (tableSerialAttribute != null) {
      entity.getAttributes().remove(tableSerialAttribute);
    }
  }

  public void setRootEntity(boolean rootEntity) {
    this.rootEntity = rootEntity;
  }

  public void setRootIdProvided(boolean rootId) {
    this.rootIdProvided = rootId;
    AttributeImpl rootIdAttribute = (AttributeImpl) entity.getAttributeByJavaName(Constants.AN_ROOTID, false);
    if (rootId) {
      if (rootIdAttribute == null) {
        rootIdAttribute = (AttributeImpl) factory.createAttribute(entity, getSourceInfo(), true);
        rootIdAttribute.setDataType(DataType.LONG_PRIMITIVE);
        rootIdAttribute.setColumnName(Constants.CN_ROOTID);
        rootIdAttribute.setName(Constants.AN_ROOTID);
        rootIdAttribute.getOptions().setComment("root entity id");
        rootIdAttribute.getOptions().setFromSuper(true);
        entity.getAttributes().add(0, rootIdAttribute);
      }
    }
    else if (rootIdAttribute != null) {
      entity.getAttributes().remove(rootIdAttribute);
    }
  }

  public void setRootClassIdProvided(boolean rootClassId) {
    this.rootClassIdProvided = rootClassId;
    AttributeImpl rootClassIdAttribute = (AttributeImpl) entity.getAttributeByJavaName(Constants.AN_ROOTCLASSID, false);
    if (rootClassId) {
      if (rootClassIdAttribute == null) {
        rootClassIdAttribute = (AttributeImpl) factory.createAttribute(entity, getSourceInfo(), true);
        rootClassIdAttribute.setDataType(DataType.INTEGER_PRIMITIVE);
        rootClassIdAttribute.setColumnName(Constants.CN_ROOTCLASSID);
        rootClassIdAttribute.setName(Constants.AN_ROOTCLASSID);
        rootClassIdAttribute.getOptions().setComment("root entity class id");
        rootClassIdAttribute.getOptions().setFromSuper(true);
        entity.getAttributes().add(0, rootClassIdAttribute);
      }
    }
    else if (rootClassIdAttribute != null) {
      entity.getAttributes().remove(rootClassIdAttribute);
    }
  }

  public void setRemote(boolean remote) {
    this.remote = remote;
  }

  public void setProvided(boolean provided) {
    this.provided = provided;
  }
}
