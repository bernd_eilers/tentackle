/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.tentackle.maven.plugin.wizard;

/**
 * Constants and strings.
 */
public class Constants {

  /** classname suffix for the domain interface. */
  public static final String DOMAIN_SUFFIX = "Domain";

  /** classname suffix for the domain implementation. */
  public static final String DOMAIN_IMPL_SUFFIX = "DomainImpl";

  /** classname suffix for the persistence interface. */
  public static final String PERSISTENCE_SUFFIX = "Persistence";

  /** classname suffix for the persistence implementation. */
  public static final String PERSISTENCE_IMPL_SUFFIX = "PersistenceImpl";

  /** java file extension. */
  public static final String JAVA_EXT = ".java";

  /** classID status file extension. */
  public static final String CLASSID_EXT = ".classid";

  /** template category for PDOs. */
  public static final String CATEGORY_PDO = "pdo";

  /** template category for operations. */
  public static final String CATEGORY_OPERATION = "operation";

  private Constants() {}
}
