/*
 * Tentackle - https://tentackle.org.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package org.tentackle.fx.component.config;

import javafx.scene.control.CheckBox;

import org.tentackle.fx.ConfiguratorService;
import org.tentackle.fx.FxControl;

/**
 * Configures a CheckBox.
 *
 * @author harald
 * @param <T> the checkbox type
 */
@ConfiguratorService(CheckBox.class)
public class CheckBoxConfigurator<T extends CheckBox> extends ButtonBaseConfigurator<T> {

  @Override
  public void configure(T control) {
    super.configure(control);
    // make change visible immediately (not only after focus lost)
    control.setOnAction(e -> updateModelAndView((FxControl) control));
  }


  /**
   * Updates the model and the view.
   * The view update will also trigger dynamic rendering such as mandatory/changeable.
   *
   * @param control the checkbox
   */
  protected void updateModelAndView(FxControl control) {
    control.updateModel();
    control.updateView();
  }

}
