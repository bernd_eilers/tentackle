@{include $currentDir/header.incl}@
@{comment
<strong>({\@code \@wurblet})</strong> Generate code to check whether a low-level persistent object is referenced.
<p>
usage:<br>
&#064;wurblet &lt;tag&gt; PdoIsReferencing [--private] [--append=&lt;sqltext&gt;] [--classvar=&lt;classvariables&gt;] &lt;expression&gt;
<p>
arguments:
<ul>
<li><em>--private:</em> makes the method private (default is public).</li>
<li><em>--append=&lt;sqltext&gt;:</em> appends an sql-string.</li>
<li><em>--classvar=&lt;classvariables&gt;:</em> reference to classvariables, if pick the statement-IDs from there.</li>
<li><em>&lt;expression&gt;:</em> see {\@link WurbletArgumentParser}.</li>
</ul>
For more options, see {\@link DbModelWurblet}.
}@
@[
  private void wurbel() throws WurbelException {

    assertEntityIsPersistable();

    String className    = getClassName();
    String methodName   = getMethodName();
    String pdoName      = getPdoClassName();

    String  classVar    = null;
    String  append      = null;
    String  scope       = "public";

    for (String arg: getOptionArgs())  {
      if (arg.equals("private")) {
        scope = "private";
      }
      else if (arg.startsWith("append=")) {
        append = arg.substring(7);
      }
      else if (arg.startsWith("classvar="))  {
        classVar = arg.substring(9);
      }
    }

    if (scope.equals("private")) {
      setRemote(false);
    }

    if (classVar == null) {
      classVar = "";
    }
    else  {
      classVar += ".";    // add dot to access class var
    }

    if (methodName == null)     throw new WurbelException("can't determine <methodname>");
    if (className == null)      throw new WurbelException("can't determine <classname>");
    if (pdoName == null)        throw new WurbelException("can't determine PDO interface");
    if (getExpressionArguments().isEmpty()) {
      throw new WurbelException("no keys given");
    }

    String params = buildMethodParameters();
    String iparms = buildInvocationParameters();
    String statementId = createStatementId();

    Entity selectingEntity = null;
    for (WurbletArgument key : getExpressionArguments()) {
      if (selectingEntity == null) {
        selectingEntity = key.getAttribute().getEntity();
      }
      else if (!selectingEntity.equals(key.getAttribute().getEntity())) {
        throw new WurbelException("all keys must belong to the same entity");
      }
    }

]@
  @(scope)@ boolean @(methodName)@(@(params)@) {
@[
    if (isRemote()) {
      // create includes
      RemoteIncludes genInc = new RemoteIncludes(this);
      PrintStream implOut   = genInc.getImplStream();
      PrintStream remoteOut = genInc.getRemoteStream();
]@
    if (getSession().isRemote())  {
      try {
@[
      if (getContextAttribute() != null)  {
]@
@{to remoteOut}@
  boolean @(methodName)@(@(pcs(params, "DomainContext context"))@) throws RemoteException;
@{to implOut}@

  \@Override
  public boolean @(methodName)@(@(pcs(params, "DomainContext context"))@) throws RemoteException {
    try {
      setDomainContext(context);
      return dbObject.@(methodName)@(@(iparms)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@
        return getRemoteDelegate().@(methodName)@(@(pcs(iparms, "getDomainContext()"))@);
@[
      } // end if contextLine != null
      else  {
]@
@{to remoteOut}@
  boolean @(methodName)@(@(params)@) throws RemoteException;
@{to implOut}@

  \@Override
  public boolean @(methodName)@(@(params)@) throws RemoteException {
@{to implOut}@
    try {
      return dbObject.@(methodName)@(@(iparms)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@
        return getRemoteDelegate().@(methodName)@(@(iparms)@);
@[
      }
]@
      }
      catch (RemoteException e) {
        throw PersistenceException.createFromRemoteException(getSession(), e);
      }
    }
@[
    } // end if remote

    String cvars = "false";
    if (getEntity().getHierarchyInheritanceType() == InheritanceType.MULTI) {
      cvars = deriveClassNameForEntity(selectingEntity) + ".CLASSVARIABLES, false";
    }
]@
    PreparedStatementWrapper st = getPreparedStatement(@(classVar)@@(statementId)@,
      () -> {
        StringBuilder sql = createSelectIdInnerSql(@(cvars)@);
@{include $currentDir/genwhere.incl}@
        getBackend().buildSelectSql(sql, false, 1, 0);
@[
    if (append != null) {
]@
        sql.append(' ');
        sql.append(@(append)@);
@[
    }
]@
        return sql.toString();
      }
    );
    int ndx = getBackend().setLeadingSelectParameters(st, 1, 0);
@{include $currentDir/gensetpar.incl}@
    getBackend().setTrailingSelectParameters(st, ndx, 1, 0);
    try (ResultSetWrapper rs = st.executeQuery()) {
      return rs.next();
    }
  }
@[
    if (classVar.length() == 0) {
]@

  private static final StatementId @(statementId)@ = new StatementId();
@[
    }
  }
]@