@{include $currentDir/header.incl}@
@{config pathAllowed}@
@{comment
<strong>({\@code \@wurblet})</strong> Generate code to select a list of low-level persistent objects.
<p>
usage:<br>
&#064;wurblet &lt;tag&gt; DbSelectList
    [--private] [--append=&lt;sqltext&gt;] [--classvar=&lt;classvariables&gt;]
    [--sort] [--lock] [--resultset] [--bounded] [--limit] [--offset]
    &lt;expression&gt;
<p>
arguments:
<ul>
<li><em>--private:</em> makes the method private (default is public).</li>
<li><em>--append=&lt;sqltext&gt;:</em> appends an sql-string.</li>
<li><em>--classvar=&lt;classvariables&gt;:</em> reference to classvariables, if pick the statement-IDs from there.</li>
<li><em>--sort:</em> sort according to orderBy()-method.</li>
<li><em>--lock:</em> generates SELECT FOR UPDATE.</li>
<li><em>--limit:</em> adds a parameter to limit the number of objects.</li>
<li><em>--offset:</em> adds a parameter to skip the first objects.</li>
<li><em>--resultset:</em> returns the ResultSetWrapper instead of a List.</li>
<li><em>--bounded:</em> returns a bounded List&lt;? extends mainClass&gt; instead of List&lt;mainClass&gt;.</li>
<li><em>&lt;expression&gt;:</em> see {\@link WurbletArgumentParser}.</li>
</ul>
For more options, see {\@link DbModelWurblet}.
}@
@[
  private void wurbel() throws WurbelException {

    String className = getClassName();
    String methodName = getMethodName();

    String  scope       = "public";
    String  classVar    = null;
    boolean sort        = false;
    boolean lock        = false;
    String  append      = null;
    boolean limit       = false;
    boolean offset      = false;
    boolean bounded     = false;
    boolean resultSet   = false;

    for (String arg: getOptionArgs())  {
      if (arg.equals("private")) {
        scope = "private";
        setRemote(false);
      }
      else if (arg.equals("sort")) {
        if (isWithDefaultSorting()) {
          if (isWithSorting()) {
            throw new WurbelException("either default sorting (--sort) or explicit sorting (+/-AN) allowed");
          }
          sort = true;
        }
        else {
          throw new WurbelException("no sorting configured in model for " + getEntity());
        }
      }
      else if (arg.equals("lock")) {
        lock = true;
      }
      else if (arg.equals("limit")) {
        limit = true;
      }
      else if (arg.equals("offset")) {
        offset = true;
      }
      else if (arg.equals("bounded")) {
        bounded = true;
      }
      else if (arg.equals("resultset")) {
        resultSet = true;
      }
      else if (arg.startsWith("append=")) {
        append = arg.substring(7);
      }
      else if (arg.startsWith("classvar="))  {
        classVar = arg.substring(9);
      }
    }

    if (resultSet) {
      setRemote(false);     // disable remoting for sure
    }

    if (classVar == null) {
      classVar = "";
    }
    else  {
      classVar += ".";    // add dot to access class var
    }

    String params = buildMethodParameters(limit, offset);
    String iparms = buildInvocationParameters(limit, offset);
    String statementId = createStatementId();

    String listType     = resultSet ? "ResultSetWrapper" :
                            ((isTracked() ? "TrackedArrayList" : "List") + "<" + (bounded ? "? extends " : "") + className + ">");
    String varListType  = (isTracked() ? "TrackedArrayList" : "List") + "<" + className + ">";
    String rmiListType  = (isTracked() ? "TrackedArrayList" : "List") + "<" + (bounded ? "? extends " : "") + (isGenerified() ? "P" : className) + ">";

]@
  @(scope)@ @(listType)@ @(methodName)@(@(params)@) {
    Db db = getSession();
@[
    if (isRemote()) {
      // create includes
      RemoteIncludes genInc = new RemoteIncludes(this);
      PrintStream implOut      = genInc.getImplStream();
      PrintStream remoteOut    = genInc.getRemoteStream();
]@
    if (db.isRemote())  {
      try {
@{to remoteOut}@
  @(rmiListType)@ @(methodName)@(@(params)@) throws RemoteException;
@{to implOut}@

  \@Override
  public @(rmiListType)@ @(methodName)@(@(params)@) throws RemoteException {
    try {
      return dbObject.@(methodName)@(@(iparms)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@
        @(listType)@ list = getRemoteDelegate().@(methodName)@(@(iparms)@);
        db.applyTo(list);
        return list;
      }
      catch (RemoteException e) {
        throw PersistenceException.createFromRemoteException(db, e);
      }
    }
@[
    } // end if remote
]@
    PreparedStatementWrapper st = getPreparedStatement(@(classVar)@@(statementId)@,
      () -> {
        StringBuilder sql = createSelectAllInnerSql();
@{include $currentDir/genwhere.incl}@
        getBackend().buildSelectSql(sql, @(lock ? "true" : "false")@, @(limit ? "limit" : "0")@, @(offset ? "offset" : "0")@);
@[
    if (sort) {
]@
        String orderSuffix = orderBy();
        if (orderSuffix != null && !orderSuffix.isEmpty()) {
          sql.append(Backend.SQL_ORDERBY).append(orderSuffix);
        }
@[
    }
    if (append != null)  {
]@
        sql.append(' ');
        sql.append(@(append)@);
@[
    }
]@
        return sql.toString();
      }
    );
@[
    if (limit || offset) {
]@
    int ndx = getBackend().setLeadingSelectParameters(st, @(limit ? "limit" : "0")@, @(offset ? "offset" : "0")@);
@[
    }
    else  {
]@
    int ndx = 1;
@[
    }
]@
@{include $currentDir/gensetpar.incl}@
@[
    if (limit || offset) {
]@
    getBackend().setTrailingSelectParameters(st, ndx, @(limit ? "limit" : "0")@, @(offset ? "offset" : "0")@);
@[
    }

    if (resultSet) {
]@
    return st.executeQuery();
@[
    }
    else  {
]@
    try (ResultSetWrapper rs = st.executeQuery()) {
@[
      if (isTracked())  {
]@
      @(varListType)@ list = new TrackedArrayList<>();
@[
      }
      else  {
]@
      @(varListType)@ list = new ArrayList<>();
@[
      }

    // optimization: newInstance() is 10 times slower than new BlahObject()
]@
      boolean derived = getClass() != @(className)@.class;
      while (rs.next()) {
        @(className)@ obj = derived ? newInstance() : new @(className)@(db);
        list.add(obj.readFromResultSetWrapper(rs));
      }
@[
      if (isTracked())  {
]@
      list.setModified(false);
@[
      }
]@
      return list;
    }
@[
    }
]@
  }
@[
    if (classVar.length() == 0) {
]@

  private static final StatementId @(statementId)@ = new StatementId();

@[
    }
  }
]@
