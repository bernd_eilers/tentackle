/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.pdo;

import org.tentackle.session.RemoteSession;

/**
 * Extended remote session providing PDO-related services.
 * 
 * @author harald
 */
public interface PdoRemoteSession extends RemoteSession {

  /**
   * Gets an extension object.<br>
   * Extensions are used to provide additional services.
   * The given interface is checked against execution permission
   * within the requested domain context. If refused, a SecurityException is thrown.
   *
   * @param <T> the extension type
   * @param context the domain context
   * @param extIf the extension interface
   * @return the extension
   */
  <T extends DomainContextDependable> T getExtension(DomainContext context, Class<T> extIf);

}
