/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


package org.tentackle.test.pdo.pdofactory;

import org.tentackle.test.pdo.mock.MockDomainObject;
import org.testng.Reporter;


/**
 * Invoice domain implementation.
 *
 * @author harald
 */
public class InvoiceDomainImpl<T extends Invoice<T>> extends MockDomainObject<T, InvoiceDomainImpl<T>> implements InvoiceDomain<T> {

  /**
   * Creates an invoice domain object.
   *
   * @param pdo the invoice
   */
  public InvoiceDomainImpl(T pdo) {
    super(pdo);
    Reporter.log("InvoiceDomainImpl created<br/>");
  }

  @Override
  public String getSumAsString() {
    return "1234.00";
  }

  @Override
  public String toString() {
    return "BlahDomain";
  }

  @Override
  public Object getUniqueDomainKey() {
    return getPdo().getInvoiceNo();
  }

}
