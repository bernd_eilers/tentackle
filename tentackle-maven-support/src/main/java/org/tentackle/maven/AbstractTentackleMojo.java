/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


package org.tentackle.maven;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.crypto.SettingsDecrypter;
import org.apache.maven.toolchain.Toolchain;
import org.apache.maven.toolchain.ToolchainManager;
import org.wurbelizer.misc.Verbosity;

import org.tentackle.common.Settings;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Base tentackle mojo.
 *
 * @author harald
 */
public abstract class AbstractTentackleMojo extends AbstractMojo {

  /**
   * The Maven Project.
   */
  @Parameter(defaultValue = "${project}",
             readonly = true,
             required = true)
  private MavenProject mavenProject;

  /**
   * The settings.<br>
   * From settings.xml.
   */
  @Parameter(defaultValue = "${settings}", readonly = true)
  private org.apache.maven.settings.Settings settings;

  @Component
  private SettingsDecrypter settingsDecrypter;

  /**
   * The current build session instance.<br>
   * Used for toolchain manager API calls.
   */
  @Parameter(defaultValue = "${session}",
      readonly = true,
      required = true)
  private MavenSession mavenSession;

  /**
   * Toolchain manager to locate tools.
   */
  @Component
  private ToolchainManager toolchainManager;


  /**
   * The verbosity.<br>
   * One of "default", "info" or "debug".
   * Debug is also turned on (if not set explicitly) by Maven's global debug flag (see command line switch <code>-X</code>).
   */
  @Parameter(defaultValue = "${tentackle.verbosity}")
  protected String verbosity;

  /**
   * Skips processing.<br>
   * Defaults to true if packaging is "pom".
   */
  @Parameter
  private Boolean skip;

  /**
   * The encode used to read and write files.
   */
  @Parameter(defaultValue = "${project.build.sourceEncoding}")
  protected String charset;   // don't use the name "encoding" as this is handled by maven


  /**
   * mapped verbosity level.
   */
  protected Verbosity verbosityLevel;

  /**
   * List of resource dirs.
   */
  protected List<String> resourceDirs;

  /**
   * The toolfinder.
   */
  private ToolFinder toolFinder;


  /**
   * Gets the maven project.
   *
   * @return the project, never null
   */
  public MavenProject getMavenProject() {
    return mavenProject;
  }

  /**
   * Gets the maven settings.
   *
   * @return the settings from settings.xml
   */
  public org.apache.maven.settings.Settings getSettings() {
    return settings;
  }

  /**
   * Decrypter for credentials in settings.xml.
   *
   * @return the settings decrypter
   */
  public SettingsDecrypter getSettingsDecrypter() {
    return settingsDecrypter;
  }

  /**
   * Gets the maven session.
   *
   * @return the session, never null
   */
  public MavenSession getMavenSession() {
    return mavenSession;
  }

  /**
   * Gets the toolchain manager.
   *
   * @return the manager, never null
   */
  public ToolchainManager getToolchainManager() {
    return toolchainManager;
  }

  /**
   * Gets the toolchain.
   *
   * @return the toolchain, null if none
   */
  public Toolchain getToolChain() {
    Toolchain toolChain = toolchainManager.getToolchainFromBuildContext("jdk", mavenSession);
    if (toolChain != null) {
      getLog().debug("using toolchain " + toolChain);
    }
    return toolChain;
  }


  @Override
  public final void execute() throws MojoExecutionException, MojoFailureException {
    prepareExecute();
    if (validate()) {
      executeImpl();
      finishExecute();
    }
  }

  /**
   * Prepares the execution.<br>
   * Invoked before validate().
   *
   * @throws MojoExecutionException if an unexpected problem occurs.
   * @throws MojoFailureException if an expected problem occurs.
   */
  public void prepareExecute() throws MojoExecutionException, MojoFailureException {
    // default does nothing
  }

  /**
   * Implements the execution.<br>
   * TT-mojos must override this method instead of {@link #execute()}. This method
   * is only invoked if validation succeeds.
   *
   * @throws MojoExecutionException if an unexpected problem occurs
   * @throws MojoFailureException if an expected problem (such as a compilation failure) occurs
   * @see #prepareExecute()
   */
  public abstract void executeImpl() throws MojoExecutionException, MojoFailureException;

  /**
   * Finishes the execution.<br>
   * The method is invoked after {@link #executeImpl()}.
   *
   * @throws MojoExecutionException if an unexpected problem occurs.
   * @throws MojoFailureException if an expected problem occurs.
   */
  public void finishExecute() throws MojoExecutionException, MojoFailureException {
    // default does nothing
  }


  /**
   * Determines the encode charset.
   */
  public void determineEncoding() {
    if (charset != null) {
      Charset cs = Charset.forName(charset);
      Settings.setEncodingCharset(cs);
      org.wurbelizer.misc.Settings.setEncodingCharset(cs);
    }
  }


  /**
   * sets the verbosity.
   */
  public void determineVerbosity() {
    if (verbosity != null) {
      try {
        verbosityLevel = Verbosity.valueOf(verbosity.toUpperCase());
      }
      catch (RuntimeException ex) {
        verbosityLevel = Verbosity.DEFAULT;
      }
    }
    else  {
      // use maven's global setting
      verbosityLevel = getLog().isDebugEnabled() ? Verbosity.DEBUG : Verbosity.DEFAULT;
    }
  }


  /**
   * Gets the path relative to the basedir.<br>
   * Parent dirs of the basedir will also be tried.
   *
   * @param path the absolute path
   * @return the shortened path
   */
  public String getPathRelativeToBasedir(String path) {
    String basePath = mavenProject.getBasedir().getAbsolutePath();
    while (basePath.length() > 0) {
      if (path.startsWith(basePath)) {
        int len = basePath.lastIndexOf(File.separatorChar);
        if (len > 0) {
          len++;    // include the last dir of the basepath
        }
        else  {
          len = basePath.length() + 1;
        }
        return path.substring(len);
      }
      // continue with the parent dir
      int ndx = basePath.lastIndexOf(File.separatorChar);
      if (ndx > 0) {
        basePath = basePath.substring(0, ndx);
      }
      else  {
        break;
      }
    }
    return path;
  }


  /**
   * Finds all resource directories.
   */
  public void findResourceDirs() {
    resourceDirs = new ArrayList<>();
    for (Object resource : mavenProject.getBuild().getResources()) {
      if (resource instanceof Resource) {
        String name = ((Resource) resource).getDirectory();
        if (name != null) {
          getLog().debug("found resource directory " + name);
          resourceDirs.add(name);
        }
      }
    }
  }


  /**
   * Checks if given directory resides in resources.
   *
   * @param dirName the directory name
   * @return the resource dir, null if not a resource
   */
  public String getResourceDirName(String dirName) {
    if (resourceDirs != null) {
      for (String resourceDir: resourceDirs) {
        if (dirName.startsWith(resourceDir)) {
          return resourceDir;
        }
      }
    }
    return null;
  }


  /**
   * Gets the canonical path of diven directory.
   *
   * @param dir the directory
   * @return the path
   * @throws MojoExecutionException if failed
   */
  public String getCanonicalPath(File dir) throws MojoExecutionException {
    try {
      return dir.getCanonicalPath();
    }
    catch (IOException ex) {
      throw new MojoExecutionException("cannot determine canonical path of " + dir, ex);
    }
  }


  /**
   * Checks the configuration.<br>
   *
   * @return true if continue with execution, false to skip
   * @throws MojoExecutionException if validation failed
   */
  protected boolean validate() throws MojoExecutionException {
    determineEncoding();
    determineVerbosity();
    if (mavenProject.getBasedir() == null) {
      throw new MojoExecutionException("missing project.baseDir");
    }
    if (skip == null && "pom".equals(mavenProject.getPackaging())) {
      skip = true;    // nothing to do here
    }
    if (Boolean.TRUE.equals(skip)) {
      getLog().info("skipped");
      return false;
    }
    return true;
  }


  /**
   * Gets the toolfinder to locate tools like java, jdeps, jlink, etc.
   *
   * @return the toolfinder
   */
  public ToolFinder getToolFinder() {
    if (toolFinder == null) {
      toolFinder = new ToolFinder(getToolChain());
    }
    return toolFinder;
  }

  /**
   * Loads a resource file into a string.
   *
   * @param path the path to the resource file
   * @return the contents
   * @throws MojoExecutionException if no such resource
   */
  public String loadResourceFileIntoString(String path) throws MojoExecutionException {
    InputStream inputStream = getClass().getResourceAsStream(path);
    if (inputStream == null) {
      throw new MojoExecutionException("no such resource: " + path);
    }
    BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
    return buffer.lines().collect(Collectors.joining(System.getProperty("line.separator")));
  }

}
