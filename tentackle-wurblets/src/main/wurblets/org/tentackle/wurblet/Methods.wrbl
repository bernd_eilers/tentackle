@{include $currentDir/header.incl}@
@{comment
<strong>({\@code \@wurblet})</strong> Generate getter and setter definitions of an entity.
<p>
usage:<br>
&#064;wurblet &lt;tag&gt; Methods [--noudk]
<p>
arguments:
<ul>
<li><em>--noudk:</em> don't generate selectByUniqueDomainKey method definition.</li>
</ul>
For more options, see {\@link ModelWurblet}.
}@
@[
  private void wurbel() throws ModelException, WurbelException {

    boolean generateSelectByUDK = getOption("noudk") == null;

    for (Attribute attr: getEntity().getAttributes())  {

      if (attr.isHidden() || attr.getOptions().isMute()) {
        continue; // skip
      }

      if (attr.getOptions().isNoMethod())    {
]@

  // no accessor methods for @(attr)@
  // @(attr.getOptions().getComment())@
@[
      }
      else {
        String type = attr.getJavaType();
        String methodSuffix = attr.getMethodNameSuffix();
        String getter = attr.getGetterName();
        String setter = attr.getSetterName();
        if (attr.getOptions().isWriteOnly()) {
]@

  // @(attr)@ is writeonly, no getter.
@[
        }
        else  {
]@

  /**
   * Gets the attribute @(attr)@.
   *
   * \@return @(attr.getOptions().getComment())@
   */
  \@Persistent(ordinal=@(attr.getOrdinal())@, comment=@(StringHelper.toDoubleQuotes(attr.getOptions().getComment()))@)
@[
          for (String annotation: attr.getOptions().getAnnotations()) {
]@
  @(annotation)@
@[
          }
          if (attr.getOptions().isBindable()) {
]@
  @(attr.getBindableAnnotation())@
@[
          }
          if (attr.getOptions().isDomainKey()) {
]@
  \@DomainKey
@[
          }
]@
  @(type)@ @(getter)@();
@[
        }

        if (attr.getOptions().isReadOnly()) {
]@

  // @(attr)@ is readonly, no setter.
@[
        }
        else {
          // check to make setter for lazy or eager object relations hidden to the application
          Relation rel = attr.getRelation();
          if (rel == null || rel.getSelectionType() == SelectionType.ALWAYS) {
]@

  /**
   * Sets the attribute @(attr)@.
   *
   * \@param @(attr.getName())@ @(attr.getOptions().getComment())@
   */
@[
            if (attr.getOptions().isBindable()) {
]@
  \@Bindable
@[
            }
]@
  void @(setter)@(@(type)@ @(attr.getName())@);
@[
          }
          if (getEntity().getOptions().getTrackType().isAttracked()) {
]@

  /**
   * Gets the modification state of @(attr)@.
   *
   * \@return true if modified
   */
  boolean is@(methodSuffix)@Modified();
@[
          }
          if (getEntity().getOptions().getTrackType() == TrackType.FULLTRACKED) {
]@

  /**
   * Gets the last persisted value of @(attr)@.
   *
   * \@return the last persisted value
   */
  @(attr.getJavaType())@ @(getter)@Persisted();
@[
          }
        }
        if (attr.getOptions().isWithTimezone()) {
]@

  /**
   * Gets the database timezone for @(attr)@.
   *
   * \@return the timezone configuration to read/write from/to the backend.
   */
  Calendar get@(methodSuffix)@TimezoneConfig();
@[
        }
      }
    }

    if (isPdo() && generateSelectByUDK) {
      List<Attribute> udk = getEntity().getUniqueDomainKey();
      if (!udk.isEmpty()) {
        List<Attribute> superUdk = null;
        if (getEntity().getSuperEntity() != null) {
          superUdk = getEntity().getSuperEntity().getUniqueDomainKey();
        }
        if (superUdk == null || !udk.equals(superUdk)) {
]@

  /**
   * Selects @(getEntity())@ by its unique domain key.
   *
@[
          String returnType = isGenerified() ? "T" : getPdoClassName();
          StringBuilder params = new StringBuilder();
          for (Attribute attr: udk) {
            appendCommaSeparated(params, attr.getJavaType());
            params.append(' ').append(attr.getName());
]@
   * \@param @(attr.getName())@ @(attr.getOptions().getComment())@
@[
          }
]@
   * \@return the @(getEntity())@, null if no such PDO
   */
  @(returnType)@ selectByUniqueDomainKey(@(params)@);
@[
        }
      }
    }
  }
]@