/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


package org.tentackle.wurblet;

import org.wurbelizer.wurbel.JavaSourceType;
import org.wurbelizer.wurbel.WurbelException;
import org.wurbelizer.wurbel.WurbelTerminationException;
import org.wurbelizer.wurbel.Wurbler;
import org.wurbelizer.wurblet.AbstractJavaWurblet;
import org.wurbelizer.wurblet.AbstractWurblet;

import org.tentackle.common.Constants;
import org.tentackle.common.StringHelper;
import org.tentackle.model.Attribute;
import org.tentackle.model.Entity;
import org.tentackle.model.EntityAliases;
import org.tentackle.model.MethodArgument;
import org.tentackle.model.Model;
import org.tentackle.model.ModelDefaults;
import org.tentackle.model.ModelException;
import org.tentackle.model.Relation;
import org.tentackle.model.RelationType;
import org.tentackle.sql.Backend;
import org.tentackle.sql.BackendFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * Extended {@link AbstractJavaWurblet} providing basic functionality for the persistent object model.
 * <p>
 * The following wurblet options are parsed:
 * <ul>
 * <li><em>--method=&lt;method-name&gt;:</em> optional name of the generated method. Defaults to the wurblet tag.</li>
 * <li><em>--model=&lt;mapping&gt;:</em> optional model mapping. Derived from source file, if missing.</li>
 * <li><em>--remote:</em> force generating remote code. Taken from model-defaults, if missing.</li>
 * <li><em>--noremote:</em> don't generate remote code. Taken from model-defaults, if missing.</li>
 * </ul>
 * For the general form of a wurblet-anchor, see {@link AbstractWurblet}.
 * @author  harald
 */
public class ModelWurblet extends AbstractJavaWurblet {

  private static final Pattern ANNOTATION_PATTERN = Pattern.compile("\\s*([\\w\\.]+)\\.class");  // only 1-line annos for now

  /**
   * The name of the model directory.
   * This is the model of the project's current (maven) module.
   */
  private String modelDirName;

  /**
   * Names of other model dirs.
   * Those models will be loaded *before* the model in modelDirName and
   * represent models of other modules that are dependencies of the current module
   * (i.e. the current model refers to).
   * <p>
   * Important: the modelDefaults and entityAliases must be the same for all models
   * including the current!
   */
  private List<String> otherModelDirNames;

  /** name of the model. */
  private String modelName;

  /** the model defaults. */
  private ModelDefaults modelDefaults;

  /** the original parsed mapping. */
  private Entity entity;

  /** wurblet specific arguments. */
  private List<String> args;

  /** list of wurblet args starting with -- (-- cut off). */
  private List<String> optionArgs;

  /** list of regular wurblet args (all args except the options). */
  private List<String> wurbletArgs;

  /** != null if detected whether this is a pdo or not. */
  private Boolean isPdo;

  /** true if isPdo != null and wurblet anchor is declared within an interface. */
  private boolean isInterface;

  /** true if --remote option set. */
  private boolean remote;

  /** the pdo classname if it's a pdo. */
  private String pdoClassName;

  /** true if missing model file is ok. */
  private boolean missingModelOk;



  /**
   * Creates a wurblet.
   */
  public ModelWurblet() {
    super();
  }

  /**
   * Gets the name of the model directory.
   *
   * @return the model dir name
   */
  public String getModelDirName() {
    return modelDirName;
  }

  /**
   * Gets the optional model defaults.
   *
   * @return the defaults, null if none
   */
  public ModelDefaults getModelDefaults() {
    return modelDefaults;
  }

  /**
   * Gets the pdo class from the source.<br>
   * Looks for annotations {@code @DomainObjectService}, {@code @PersistentObjectService} and interface extensions.
   *
   * @return the pdo name
   * @throws WurbelException if pdo class cannot be determined from the source file
   */
  public String getPdoClassName() throws WurbelException {
    if (isPdo == null)  {   // not determined yet
      isPdo = false;

      for (int ndx=0; ; ndx++) {
        String annotation = getContainer().getProperty(Wurbler.PROPSPACE_WURBLET, JavaSourceType.ANNOTATION + ndx);
        if (annotation == null) {
          break;
        }
        if (annotation.startsWith("@DomainObjectService") ||
            annotation.startsWith("@PersistentObjectService")) {
          Matcher matcher = ANNOTATION_PATTERN.matcher(annotation);
          if (matcher.find()) {
            pdoClassName = matcher.group(1);
            if (getContainer().getVerbosity().isDebug()) {
              getContainer().getLogger().info("pdoClassName: " + pdoClassName);
            }
            isPdo = true;
            return pdoClassName;
          }
          throw new WurbelException("malformed annotation: " + annotation);
        }
      }

      // may be an interface that extends PersistentObject<Blah>
      String persistentIface = getContainer().getProperty(Wurbler.PROPSPACE_WURBLET, JavaSourceType.EXTENDS + 0);
      if (persistentIface != null) {
        int ndx = persistentIface.indexOf('<');
        if (ndx > 0) {
          persistentIface = persistentIface.substring(ndx + 1);
          ndx = persistentIface.lastIndexOf('>');
          if (ndx > 0) {
            pdoClassName = persistentIface.substring(0, ndx);
            if (pdoClassName.length() > 1 && pdoClassName.indexOf('<') < 0) {   // not T or some other generic type
              isPdo = true;
              isInterface = true;
              return pdoClassName;
            }
          }
        }
      }

      // may be an abstract class (if inheritance is used)
      pdoClassName = getContainer().getProperty(Wurbler.PROPSPACE_WURBLET, JavaSourceType.DEFINITION);
      if (pdoClassName != null) {
        /*
         * If something of:
         * extends AbstractPersistentObject<Adresse, AdressePersistenceImpl> implements AdressePersistence
         * or
         * extends UmzugsListePersistenceImpl<UmzugsErfassungsListe, UmzugsErfassungsListePersistenceImpl>
         *   implements UmzugsErfassungsListePersistence
         * or
         * <T extends UmzugsListe<T>, P extends UmzugsListePersistenceImpl<T,P>>
         *   extends AbstractPersistentObject<T,P> implements UmzugsListePersistence<T>
         */
        if (getContainer().getVerbosity().isDebug()) {
          getContainer().getLogger().info(getContainer().getProperty(Wurbler.PROPSPACE_WURBLET, JavaSourceType.CLASS_NAME) +
                                          ": definition = '" + pdoClassName + "'");
        }
        int ndx = pdoClassName.indexOf("extends");
        if (ndx >= 0) {
          int ndx1 = pdoClassName.indexOf('<');
          int ndx2 = pdoClassName.indexOf(',');
          if (ndx1 > ndx && ndx2 > ndx1) {
            pdoClassName = pdoClassName.substring(ndx1 + 1, ndx2).trim();
            isPdo = true;
            return pdoClassName;
          }
        }
        ndx = pdoClassName.indexOf("T extends");
        if (ndx >= 0) {
          pdoClassName = pdoClassName.substring(ndx + 9);
          ndx = pdoClassName.indexOf('<');
          if (ndx > 0) {
            pdoClassName = pdoClassName.substring(0, ndx).trim();
            isPdo = true;
            return pdoClassName;
          }
        }
      }
    }

    if (isPdo) {
      return pdoClassName;
    }

    // last try: use interface name (manually implemented PDOs, or migration from TT1)
    pdoClassName = getContainer().getProperty(Wurbler.PROPSPACE_WURBLET, JavaSourceType.INTERFACE_NAME);
    if (pdoClassName != null) {
      return pdoClassName;
    }

    throw new WurbelException("cannot determine the pdo-class from the java-source");
  }


  /**
   * Returns whether this is a pdo.
   *
   * @return true if pdo
   */
  public boolean isPdo() {
    if (isPdo == null) {
      try {
        getPdoClassName();    // sets isPdo!
      }
      catch (WurbelException wex) {
        // ignore
      }
    }
    return isPdo;
  }

  /**
   * Returns whether wurblet is defined within an interface.<br>
   * Only valid if isPdo() returns non-null.
   *
   * @return true if interface
   */
  public boolean isInterface() {
    return isInterface;
  }

  /**
   * Returns true if --remote option set.
   *
   * @return true if remote
   */
  public boolean isRemote() {
    return remote;
  }

  /**
   * Sets the remote option explicitly.
   *
   * @param remote true if remoting enabled
   */
  public void setRemote(boolean remote) {
    this.remote = remote;
  }

  /**
   * Returns whether the entity is part of an inheritance tree.
   *
   * @return truf if part of an inheritance tree
   */
  public boolean isPartOfInheritanceHierarchy() {
    return isPdo() && getEntity().getTopSuperEntity().isAbstract();
  }

  /**
   * Returns whether the class is defined using java generics.
   * <p>
   * Generics are used in abstract inheritable classes.
   * Final concrete PDO classes must not use generics.
   * Otherwise the generated wurblet code will not compile.
   *
   * @return true if class is generified
   */
  public boolean isGenerified() {
    String definition = getContainer().getProperty(Wurbler.PROPSPACE_WURBLET, JavaSourceType.DEFINITION);
    return definition != null && definition.startsWith("<");
  }


  /**
   * Gets the methodname.<br>
   * From the guardname or from arg "--method=&lt;.....&gt;" if present.
   *
   * @return the method name
   * @throws WurbelException if no guardname
   */
  public String getMethodName() throws WurbelException {
    String methodName = getOption("method");
    if (methodName == null) {
      methodName = getGuardName();
    }
    return methodName;
  }


  /**
   * Gets the name of the modelfile.
   *
   * @return the name
   * @throws WurbelException if model could not be determined
   */
  public String getModelName() throws WurbelException {
    if (modelName == null) {
      modelName = getOption("model");
      if (modelName == null) {
        // determine from source file
        modelName = getPdoClassName();
      }
      if (modelName == null) {
        throw new WurbelException("model not specified");
      }
    }
    return modelName;
  }


  /**
   * Applies the semantics of {@link #getClassName()} to another entity.<br>
   * Example:
   * <pre>
   * getEntity() -&gt; Firma
   * getClassName() -&gt; "MyFirmaPersistenceImpl"
   * Assumed that otherEntity = Kontakt (which is a superclass of Firma, for example), then:
   * deriveClassNameForEntity(otherEntity) -&gt; "MyKontaktPersistenceImpl"
   * </pre>
   * @param otherEntity the other entity
   * @return the derived classname
   * @throws WurbelException if this classname does not contain the entity name as a substring
   */
  public String deriveClassNameForEntity(Entity otherEntity) throws WurbelException {
    String className = getClassName();
    String entityName = getEntity().getName();
    int ndx = className.indexOf(entityName);
    if (ndx < 0) {
      throw new WurbelException(className + " does not contain the entity name '" + entityName + "' substring");
    }
    String lead = className.substring(0, ndx);
    String tail = className.substring(ndx + entityName.length());
    return lead + otherEntity.getName() + tail;
  }


  /**
   * Sorts the given list of entities by inheritance level plus classid.
   *
   * @param entities the entities
   * @return the sorted entities (same reference as argument)
   */
  public List<Entity> orderByInheritanceLevelAndClassId(List<Entity> entities) {
    entities.sort(Comparator.comparingInt(Entity::getOrdinal).thenComparingInt(Entity::getClassId));
    return entities;
  }

  /**
   * Gets the model entity.
   *
   * @return the entity
   */
  public Entity getEntity() {
    return entity;
  }

  /**
   * Gets the wurblet arguments.
   *
   * @return the wurblet args
   */
  public List<String> getArgs() {
    return args;
  }


  /**
   * Gets the wurblet options.<br>
   * The options got the leading '--' removed.
   *
   * @return the option args
   */
  public List<String> getOptionArgs() {
    return optionArgs;
  }


  /**
   * Gets the option if set.<br>
   * Options come in two flavours:
   * <ol>
   * <li>without a value. Example: --remote</li>
   * <li>with a value. Example: --model=modlog.map</li>
   * </ol>
   *
   * @param option the option
   * @return the empty string (case 1), the value (case 2) or null if option not set
   */
  public String getOption(String option) {
    int equalsOffset = option.length();
    for (String arg: getOptionArgs()) {
      if (arg.equals(option)) {
        return "";
      }
      if (arg.startsWith(option) && arg.charAt(equalsOffset) == '=') {
        return arg.substring(equalsOffset + 1);
      }
    }
    return null;
  }


  /**
   * Gets the wurblet arguments.<br>
   * All arguments except the options.
   *
   * @return the wurblet args
   */
  public List<String> getWurbletArgs() {
    return wurbletArgs;
  }


  /**
   * Creates the method name to select a relation.
   *
   * @param relation the relation
   * @return the method name
   */
  public String createRelationSelectMethodName(Relation relation) {
    StringBuilder text = new StringBuilder();
    text.append("select");

    if (relation.getMethodName() != null) {
      if (relation.getRelationType() == RelationType.LIST) {
        text.append("By");
      }
      text.append(relation.getMethodName());
    }
    else {
      if (relation.getRelationType() == RelationType.LIST) {
        text.append("By");
        for (MethodArgument arg: relation.getMethodArgs()) {
          text.append(StringHelper.firstToUpper(arg.getForeignAttribute().getName()));
        }
      }
      else {
        if (relation.isSelectionCached()) {
          text.append("Cached");
        }
      }
    }

    return text.toString();
  }


  /**
   * Creates the method name to select a relation.
   *
   * @param relation the relation
   * @return the method name
   */
  public String createListRelationDeleteMethodName(Relation relation) {
    StringBuilder text = new StringBuilder();
    text.append("deleteBy");
    if (relation.getMethodName() != null) {
      text.append(relation.getMethodName());
    }
    else  {
      for (MethodArgument arg : relation.getMethodArgs()) {
        text.append(StringHelper.firstToUpper(arg.getForeignAttribute().getName()));
      }
    }
    return text.toString();
  }


  /**
   * Creates the method argument declaration for the select or delete method.
   *
   * @param relation the relation
   * @return the argument declaration
   * @throws ModelException if failed
   */
  public String createDeclaredArgsForSelectOrDeleteMethod(Relation relation) throws ModelException {
    StringBuilder text = new StringBuilder();
    for (MethodArgument arg : relation.getMethodArgs()) {
      if (text.length() > 0) {
        text.append(", ");
      }
      Attribute attr = arg.getForeignAttribute();
      text.append(attr.getJavaType()).append(' ').append(attr.getName());
    }
    return text.toString();
  }


  /**
   * {@inheritDoc}
   * <p>
   * Overridden to load the map file.
   *
   * @throws WurbelException if running the wurblet failed
   */
  @Override
  public void run() throws WurbelException {

    String wurbletOptions = getConfiguration();
    if (wurbletOptions != null && wurbletOptions.contains("missingModelOk")) {
      missingModelOk = true;
    }

    super.run();

    args = Arrays.asList(container.getArgs());
    wurbletArgs = new ArrayList<>();
    optionArgs = new ArrayList<>();
    for (String arg : args) {
      if (arg != null) {
        if (arg.startsWith("--")) {
          optionArgs.add(arg.substring(2));
        }
        else {
          wurbletArgs.add(arg);
        }
      }
    }

    modelDirName = getContainer().getProperty(Wurbler.PROPSPACE_EXTRA, "model");
    if (modelDirName == null) {
      throw new WurbelException("missing wurblet property 'model'");
    }
    File modelDir = new File(modelDirName);
    if (!modelDir.exists()) {
      getContainer().getLogger().info("creating " + modelDir);
      modelDir.mkdirs();
    }
    if (!modelDir.isDirectory()) {
      throw new WurbelException(modelDir + " is not a directory");
    }

    String otherModels = getContainer().getProperty(Wurbler.PROPSPACE_EXTRA, "otherModels");
    if (otherModels != null) {
      otherModelDirNames = new ArrayList<>();
      StringTokenizer stok = new StringTokenizer(otherModels, " \t\n\r\f,");
      while (stok.hasMoreTokens()) {
        otherModelDirNames.add(stok.nextToken());
      }
    }

    // set the backends to validate the model, if any.
    // this is a comma separated list of backends, null if none, all for all backends in classpath
    String backends = getContainer().getProperty(Wurbler.PROPSPACE_EXTRA, "backends");
    if (backends != null) {
      Collection<Backend> backendList = new ArrayList<>();
      if ("all".equalsIgnoreCase(backends)) {
        backendList.addAll(BackendFactory.getInstance().getAllBackends());
      }
      else if (!"none".equalsIgnoreCase(backends)) {
        for (String backend: backends.split(",")) {
          backendList.add(BackendFactory.getInstance().getBackendByName(backend.trim()));
        }
      }
      Model.getInstance().getEntityFactory().setBackends(backendList);
    }

    // scan optional model defaults
    modelDefaults = null;
    String modelDefaultsStr = getContainer().getProperty(Wurbler.PROPSPACE_EXTRA, "modelDefaults");
    if (modelDefaultsStr != null) {
      try {
        modelDefaults = new ModelDefaults(modelDefaultsStr);
      }
      catch (ModelException mex) {
        throw new WurbelException(mex.getMessage(), mex);
      }
    }

    // scan optional entity aliases
    EntityAliases entityAliases = null;
    String entityAliasesStr = getContainer().getProperty(Wurbler.PROPSPACE_EXTRA, "entityAliases");
    if (entityAliasesStr != null) {
      try {
        entityAliases = new EntityAliases(entityAliasesStr);
      }
      catch (ModelException mex) {
        throw new WurbelException(mex.getMessage(), mex);
      }
    }

    // load the model (if not yet done)
    Model model = Model.getInstance();
    try {
      if (otherModelDirNames != null) {
        for (String otherModelDirName: otherModelDirNames) {
          model.loadModel(otherModelDirName, modelDefaults, entityAliases);
        }
      }
      model.loadModel(modelDirName, modelDefaults, entityAliases);
    }
    catch (ModelException mex) {
      // model exceptions cause the whole wurbel run to abort because it doesnt make any sense to clutter the sources
      WurbelException wex = new WurbelTerminationException(
              "errors in model loaded from directory '" + modelDirName + "'", true, mex);
      if (model instanceof TentackleWurbletsModel && mex.getElement() != null &&
          ((TentackleWurbletsModel) model).getLoadingException() == null) {
        // delay first exception to concrete classes if exception could be associated to entities
        ((TentackleWurbletsModel) model).setLoadingException(wex);
      }
      else  {
        throw wex;
      }
    }

    // get the entity

    try {
      if (getModelName().indexOf(File.separatorChar) >= 0) {
        // is a filename (load it if it's not already loaded)
        entity = model.loadByFileName(getModelName(), modelDefaults, entityAliases);
      }
      else  {
        // is an entity name
        entity = model.getByEntityName(getModelName());
      }
    }
    catch (ModelException mex) {
      throw new WurbelTerminationException("errors in model loaded from file '" + getModelName() + "'", mex);
    }

    if (entity == null && !missingModelOk) {
      Throwable delayedModelException = null;
      if (model instanceof TentackleWurbletsModel) {
        WurbelException wex = ((TentackleWurbletsModel) model).getLoadingException();
        if (wex != null) {
          delayedModelException = wex.getCause();
        }
      }
      throw new WurbelTerminationException("no such entity '" + getModelName() + "' in model " + modelDir, delayedModelException);
    }


    if (entity != null) {
      remote = entity.getOptions().isRemote();
    }
    else if (modelDefaults != null && modelDefaults.getRemote() != null) {
      remote = modelDefaults.getRemote();
    }

    // override global option
    if (getOption("remote") != null) {
      remote = true;
    }
    if (getOption("noremote") != null) {
      remote = false;
    }

    // Throw delayed wurbel execption if the real cause is related to this entity
    if (model instanceof TentackleWurbletsModel && entity != null) {
      WurbelException wex = ((TentackleWurbletsModel) model).getLoadingException();
      if (wex != null && wex.getCause() instanceof ModelException) {
        ModelException mex = (ModelException) wex.getCause();
        if (mex.isRelatedTo(entity)) {
          throw wex;
        }
      }
    }
  }



  // ----------------- utility methods to simplify writing wurblets ----------------------------------


  /**
   * Checks whether attribute is the pdo ID.
   *
   * @param attribute the attribute
   * @return true if pdo id
   */
  public boolean isIdAttribute(Attribute attribute) {
    return attribute.getName().equals(Constants.CN_ID);
  }

  /**
   * Checks whether attribute is the pdo serial.
   *
   * @param attribute the attribute
   * @return true if pdo serial
   */
  public boolean isSerialAttribute(Attribute attribute) {
    return attribute.getName().equals(Constants.CN_SERIAL);
  }

  /**
   * Checks whether attribute is the pdo ID or serial.
   *
   * @param attribute the attribute
   * @return true if pdo id or serial
   */
  public boolean isIdOrSerialAttribute(Attribute attribute) {
    return isIdAttribute(attribute) || isSerialAttribute(attribute);
  }

  /**
   * Checks whether attribute is derived from a superclass.
   *
   * @param attribute the attribute
   * @return true if derived from superclass
   */
  public boolean isAttributeDerived(Attribute attribute) {
    return attribute.getOptions().isFromSuper() || attribute.getEntity() != entity;
  }

  /**
   * Adds a string to a comma separated list.
   *
   * @param builder the string builder
   * @param appendStr the string to append
   */
  public void appendCommaSeparated(StringBuilder builder, String appendStr) {
    if (builder.length() > 0) {
      builder.append(", ");
    }
    builder.append(appendStr);
  }

  /**
   * Prepends a string to a comma separated list.
   *
   * @param builder the string builder
   * @param prependStr the string to prepend
   */
  public void prependCommaSeparated(StringBuilder builder, String prependStr) {
    if (builder.length() > 0) {
      builder.insert(0, ", ");
    }
    builder.insert(0, prependStr);
  }

}
