/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


package org.tentackle.validate;

import java.util.Collections;
import java.util.List;
import org.tentackle.validate.validator.AbstractObjectValidator;

/**
 * Validator implementation.
 *
 * @author harald
 */
public class TestObjectValidatorImpl extends AbstractObjectValidator<TestObjectValidator> {

  @Override
  public String getMessage() {
    return annotation.message();
  }

  @Override
  public String getErrorCode() {
    return annotation.error();
  }

  @Override
  public Class<? extends ValidationSeverity>[] getSeverity() {
    return annotation.severity();
  }

  @Override
  public int getPriority() {
    return annotation.priority();
  }

  @Override
  public Class<? extends ValidationScope>[] getScope() {
    return annotation.scope();
  }

  @Override
  public List<? extends ValidationResult> validate(ValidationContext validationContext) {
    Object object = validationContext.getObject();
    if (object == null) {
      return Collections.emptyList();
    }
    if (object instanceof TestObject) {
      if (((TestObject)object).percent % 2 == 0) {
        return Collections.emptyList();
      }
      return createFailedValidationResult("percent is not even", validationContext);
    }
    else  {
      throw new ValidationRuntimeException("not a Comparable");
    }
  }

}
