/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


package org.tentackle.apt;

import org.tentackle.common.AnnotationProcessor;

import javax.annotation.processing.SupportedAnnotationTypes;

/**
 * Processor for all interception annotations in the core package.<br>
 * Applies to interceptors applicable to interfaces extending {@code Interceptable} or
 * implementation classes of {@code Interceptable}.
 *
 * @author harald
 */
@SupportedAnnotationTypes({
  "org.tentackle.log.Log"
})
@AnnotationProcessor
public class AllInterceptorAnnotationProcessor extends AbstractInterceptorAnnotationProcessor {

  public AllInterceptorAnnotationProcessor() {
    super(Type.ALL);
  }

}
