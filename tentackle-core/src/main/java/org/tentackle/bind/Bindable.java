/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


package org.tentackle.bind;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotates a member of a class for eligibility in GUI bindings.
 * <p>
 * The class may be any kind of class (not necessarily a bean, but maybe),
 * usually a PDO or a GUI-view. The members may be regular objects
 * or primitives. Annotated objects itself may contain annotated members.
 * In fact, only the leafs of such binding hierarchies are actually bound.
 * <p>
 *
 * @author harald
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)

public @interface Bindable {

  /**
   * GUI Binding options.
   * <p>
   * Example: <tt>@Bindable(options="UC,MAXCOLS=5)</tt>
   *
   * @return the options, the empty string if none
   */
  String options() default "";

}

